package pl.radical.ttbin.records;

import lombok.Getter;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x32, length = 17)
public class TreadmillRecord implements ITTBinRecord {
    /**
     * Time stamp of this record
     */
    @Getter
    private int timeStamp;
    /**
     * Distance
     */
    @Getter
    private float distance;

    /**
     * Calories burned
     */
    @Getter
    private short calories;

    /**
     * Steps
     */
    @Getter
    private int steps;
    /**
     * Step length in cm, not implemented yet
     */
    @Getter
    private short stepLength;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        timeStamp = buffer.getInt();
        distance = buffer.getFloat();
        calories = buffer.getShort();
        steps = buffer.getInt();
        stepLength = buffer.getShort();
        return this;
    }
}
