package pl.radical.ttbin.records.types;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 25.10.2015
 */
@AllArgsConstructor
@Slf4j
public enum ActivityType {
    UNKNOWN(-1),
    RUNNING(0),
    CYCLING(1),
    SWIMMING(2),
    STOPWATCH(6), //doesn't actually log any data
    TREADMILL(7),
    FREESTYLE(8),
    GYM(9),
    INDOOR_CYCLING(11);

    private int code;

    public static ActivityType byCode(int code) {
        for (ActivityType type : ActivityType.values()) {
            if (type.code == code) {
                return type;
            }
        }
        log.warn("Unknown ActivityType was requested! [{}]", code);
        return UNKNOWN;
    }

}
