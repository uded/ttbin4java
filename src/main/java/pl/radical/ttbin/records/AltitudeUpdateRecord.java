package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3e, length = 8)
@ToString
public class AltitudeUpdateRecord implements ITTBinRecord {
    @Getter
    private short realAltitude;
    @Getter
    private float totalClimb;
    @Getter
    private byte qualifier;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        realAltitude = buffer.getShort();
        totalClimb = buffer.getFloat();
        qualifier = buffer.get();
        return this;
    }
}
