package pl.radical.ttbin;

import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;
import pl.radical.ttbin.records.HeaderRecord;
import pl.radical.ttbin.records.Record;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 18.10.2015
 */
@Slf4j
@SuppressWarnings("unchecked")
public class TTBinRecords implements Iterable<ITTBinRecord> {
    /**
     * A map holding a list of all binary tags and corresponding handlers
     */
    private static Map<Byte, Class<? extends ITTBinRecord>> handlers = new HashMap<>();

    // Pre-loading all of the classes with @Record annotation
    static {
        Reflections reflections = new Reflections(Record.class.getPackage().getName());
        log.debug("Found {} classes with appropriate annotations.", reflections.getTypesAnnotatedWith(Record.class).size());
        for (Class<?> clazz : reflections.getTypesAnnotatedWith(Record.class)) {
            log.trace("For tag {} registering handler: {}", clazz.getAnnotation(Record.class).tag(), clazz);
            if (ITTBinRecord.class.isAssignableFrom(clazz)) {
                handlers.put(clazz.getAnnotation(Record.class).tag(), (Class<? extends ITTBinRecord>) clazz);
            }
        }
    }

    /**
     * A list of actuall records parsed from the file
     */
    private final List<ITTBinRecord> records = new ArrayList<>();

    /**
     * Returns an iterator over elements of type {@code ITTBinRecord}.
     *
     * @return an Iterator.
     */
    @Override
    public Iterator<ITTBinRecord> iterator() {
        return records.iterator();
    }

    /**
     * Read and parse a TTBIN by resolving a string with file path to a {@link File}, producing a long list of {@link ITTBinRecord} objects
     *
     * @param fileName a file path to parse
     * @throws ParserException when it cannot be parsed
     */
    public void read(String fileName) throws ParserException {
        read(new File(fileName));
    }

    /**
     * Read and parse a TTBIN {@link File}, producing a long list of {@link ITTBinRecord} objects
     *
     * @param file a file to parse
     * @return this object actually
     * @throws ParserException when it cannot be parsed
     */
    public TTBinRecords read(File file) throws ParserException {
        if (!file.exists()) {
            throw new ParserException("No file can be found by given location: " + file.toString());
        }
        if (!file.canRead()) {
            throw new ParserException("File '" + file.toString() + "' cannot be read.");
        }
        // We can read the file as a whole - those are rather small files :-)
        try {
            ByteBuffer buffer = ByteBuffer.wrap(Files.readAllBytes(Paths.get(file.toURI()))).asReadOnlyBuffer().order(ByteOrder.LITTLE_ENDIAN);
            parseRecords(buffer);
            return this;
        } catch (IOException e) {
            throw new ParserException("Given file was not accesible for reading", e);
        }
    }

    public void writeDebugFile(File file) throws IOException {
        if (file.exists()) {
            file.delete();
        }
        FileWriter fw = new FileWriter(file);

        for (ITTBinRecord record : records) {
            fw.append(record.toString());
            fw.append("\n");
        }

        fw.flush();
        fw.close();
    }

    /**
     * Parsing actual methods from given buffer and representing the result as a stream of records
     *
     * @param buffer to parse
     * @throws ParserException an exception given while parsing
     */
    private void parseRecords(ByteBuffer buffer) throws ParserException {
        try {
            Map<Byte, Short> tags = Collections.emptyMap();
            while (buffer.hasRemaining()) {
                byte tag = buffer.get();
                if (handlers.containsKey(tag)) {
                    ITTBinRecord record = handlers.get(tag).newInstance().readRecord(buffer);
                    if (record != null) {
                        if (HeaderRecord.class.equals(record.getClass())) {
                            tags = ((HeaderRecord) record).getTags();
                        }
                        log.debug("{}", record);
                        records.add(record);
                    }
                } else if (!tags.isEmpty() && tags.containsKey(tag)) {
                    log.warn("UNKNOWN TAG! I am about to panic! No registered handler class for tag: {}", tag);
                    buffer.position(buffer.position() + tags.get(tag) - 1);
                } else {
                    log.error("Something went wrong - not a recognizable tag, PANICKING! [tag read: {} | buffer position for tag: {}]", tag, buffer.position() - 1);
                    throw new ParserException("Something went wrong - no recognizable tag, PANICKING!");
                }
            }
        } catch (InstantiationException e) {
            throw new ParserException("A handler for tag was not instantiated correctly", e);
        } catch (IllegalAccessException e) {
            throw new ParserException("A read method of given parser is not accessible for call", e);
        }
    }

}
