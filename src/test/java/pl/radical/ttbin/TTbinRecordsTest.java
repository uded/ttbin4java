package pl.radical.ttbin;

import junit.framework.TestCase;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 19.10.2015
 */
@RunWith(JUnitParamsRunner.class)
@Slf4j
public class TTbinRecordsTest extends TestCase {
    @Test
    @Parameters(method = "getFileList")
    public void testParse(Path filePath) throws Exception {
        log.info("Testing file ---> {}", filePath.toFile());
        String debugFile = "target/" + filePath.getFileName().toFile().toString().replace(".ttbin", ".data");
        log.info("Debug file   ---> {}", debugFile);
        new TTBinRecords().read(filePath.toFile()).writeDebugFile(new File(debugFile));
    }

    public Object[] getFileList() throws IOException {
        return Files.list(Paths.get("src/test/data/")).filter(path1 -> path1.getFileName().toString().endsWith("ttbin")).toArray();
    }
}