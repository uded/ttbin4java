package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;
import java.util.stream.IntStream;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3c, length = 41)
@ToString
public class RaceSetupRecord implements ITTBinRecord {
    /**
     * uint8_t  race_id[16];   only used for a web services race, 0 otherwise
     */
    private byte[] raceId = new byte[16];

    /**
     * float    distance;  - metres
     */
    @Getter
    private float distance;

    /**
     * uint32_t duration;  seconds
     */
    @Getter
    private int duration;

    //char     name[16];  /* unused characters are zero */
    @Getter
    private char[] unused = new char[16];

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        IntStream.range(0, 16).forEach(value -> raceId[value] = buffer.get());
        distance = buffer.getFloat();
        duration = buffer.getInt();
        IntStream.range(0, 16).forEach(value -> unused[value] = buffer.getChar());
        return this;
    }
}
