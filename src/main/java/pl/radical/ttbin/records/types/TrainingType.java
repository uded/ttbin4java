package pl.radical.ttbin.records.types;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 25.10.2015
 */
@AllArgsConstructor
@Slf4j
public enum TrainingType {
    UNKNOWN(-1),
    GOAL_DISTANCE(0),
    GOAL_TIME(1),
    GOAL_CALORIES(2),
    ZONES_PACE(3),
    ZONES_HEART(4),
    ZONES_CADENCE(5),
    RACE(6),
    LAPS_TIME(7),
    LAPS_DISTANCE(8),
    LAPS_MANUAL(9),
    STROKE_RATE(10),
    ZONES_SPEED(11),
    INTERVALS(12);

    private int code;

    public static TrainingType byCode(int code) {
        for (TrainingType type : TrainingType.values()) {
            if (type.code == code) {
                return type;
            }
        }
        log.warn("Unknown TrainingType was requested! [{}]", code);
        return UNKNOWN;
    }
}
