package pl.radical.ttbin.records.types;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 25.10.2015
 */
@AllArgsConstructor
@Slf4j
public enum StatusType {
    UNDEFINED(-1),
    READY(0),
    ACTIVE(1),
    PAUSED(2),
    STOPPED(3);

    private int code;

    public static StatusType byCode(int code) {
        for (StatusType type : StatusType.values()) {
            if (type.code == code) {
                return type;
            }
        }
        log.warn("Unknown StatusType was requested! [{}]", code);
        return UNDEFINED;
    }

}
