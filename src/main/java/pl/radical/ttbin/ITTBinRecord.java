package pl.radical.ttbin;

import java.nio.ByteBuffer;

/**
 * A generic representation of an TTBIN Record, as described by the file format. The records can differ significantly from each other,
 * but the tag is one thing they have in common;
 *
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 20.10.2015
 */
public interface ITTBinRecord<T extends ITTBinRecord> {
    T readRecord(ByteBuffer buffer);
}
