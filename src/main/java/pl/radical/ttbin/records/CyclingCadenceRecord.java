package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x31, length = 11)
@ToString
@Slf4j
public class CyclingCadenceRecord implements ITTBinRecord {
    @Getter
    private int initialPosition;

    @Getter
    private int wheelRevolution;

    @Getter
    private short wheelRevolutionTime;

    @Getter
    private short crankRevolution;

    @Getter
    private short crankRevolutionTime;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        initialPosition = buffer.position();

        wheelRevolution = buffer.getInt();
        wheelRevolutionTime = buffer.getShort();
        crankRevolution = buffer.getShort();
        crankRevolutionTime = buffer.getShort();

        return this;
    }
}
