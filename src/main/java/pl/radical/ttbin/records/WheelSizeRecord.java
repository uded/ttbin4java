package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x2b, length = 5)
@ToString
public class WheelSizeRecord implements ITTBinRecord {
    /**
     * Pool size in centimeters
     */
    @Getter
    private int wheelSize;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        wheelSize = buffer.getInt();
        return this;
    }
}
