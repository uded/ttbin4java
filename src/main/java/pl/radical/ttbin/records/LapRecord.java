package pl.radical.ttbin.records;

import lombok.Getter;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x2f, length = 11)
public class LapRecord implements ITTBinRecord {
    /**
     * total_time(seconds, since activity start)
     */
    @Getter
    private int totalTime;

    /**
     * Total distance (in meters)
     */
    @Getter
    private float totalDistance;

    /**
     * Total calories
     */
    @Getter
    private short totalCalories;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        totalTime = buffer.getInt();
        totalDistance = buffer.getFloat();
        totalCalories = buffer.getShort();
        return this;
    }
}
