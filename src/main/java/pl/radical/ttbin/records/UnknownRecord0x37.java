package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x37, length = 2)
@ToString
@Slf4j
public class UnknownRecord0x37 implements ITTBinRecord {
    @Getter
    private byte data;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        data = buffer.get();
        log.trace("Unknown record type: 0x37[55], value: {}", data);
        return this;
    }
}
