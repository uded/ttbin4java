package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 18.10.2015
 */
@Record(tag = 0x20, length = 117)
@Slf4j
@ToString(exclude = {"lengthCount", "tags"})
public class HeaderRecord implements ITTBinRecord {
    /**
     * File version - current one is 8
     */
    @Getter
    private short fileVersion;

    /**
     * Firmware version of the watch
     */
    @Getter
    private String firmwareVersion;

    /**
     * Product ID:
     * 2005 - TomTom Runner2 / Spark?
     */
    @Getter
    private short productId;

    @Getter
    private Instant startTime;
    @Getter
    private Instant watchTime;
    @Getter
    private long watchTimeOffset;

    /**
     * Number of headers defined in this version of the file
     */
    @Getter
    private short lengthCount;

    @Getter
    private Map<Byte, Short> tags = new HashMap<>();

    @Override
    public HeaderRecord readRecord(ByteBuffer buffer) {
        fileVersion = buffer.getShort();
        firmwareVersion = buffer.get() + "." + buffer.get() + "." + buffer.get() + ".0"; // TODO Split for TCX and others
        productId = buffer.getShort();

        startTime = Instant.ofEpochSecond(buffer.getInt());
        buffer.position(108);// TODO This can be parsed as well...
        watchTime = Instant.ofEpochSecond(buffer.getInt());
        watchTimeOffset = buffer.getInt(); // seconds from UTC
        buffer.get(); // Reserved byte
        lengthCount = buffer.get();

        for (int i = 0; i < lengthCount; i++) {
            tags.put(buffer.get(), buffer.getShort());
        }
        return this;
    }
}
