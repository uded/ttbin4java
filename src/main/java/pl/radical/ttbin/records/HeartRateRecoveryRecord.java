package pl.radical.ttbin.records;

import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.RecoveryStatusType;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3f, length = 9)
@ToString
public class HeartRateRecoveryRecord implements ITTBinRecord {
    /**
     * Recovery status
     */
    private RecoveryStatusType status;

    /**
     * Heartbeat difference during recovery
     */
    private int heartRate;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        status = RecoveryStatusType.byCode(buffer.getInt());
        heartRate = buffer.getInt();
        return this;
    }
}
