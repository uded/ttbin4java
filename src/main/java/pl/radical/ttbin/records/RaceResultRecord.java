package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3d, length = 11)
@ToString
public class RaceResultRecord implements ITTBinRecord {
    @Getter
    private int duration;
    @Getter
    private float distance;
    @Getter
    private short calories;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        duration = buffer.getInt();
        distance = buffer.getFloat();
        calories = buffer.getShort();
        return this;
    }
}
