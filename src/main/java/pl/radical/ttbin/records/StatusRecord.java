package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.ActivityType;
import pl.radical.ttbin.records.types.StatusType;

import java.nio.ByteBuffer;
import java.time.Instant;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 21.10.2015
 */
@ToString
@Record(tag = 0x21, length = 7)
public class StatusRecord implements ITTBinRecord {
    @Getter
    private StatusType status;
    @Getter
    private ActivityType activity;
    @Getter
    private Instant timestamp;

    @Override
    public StatusRecord readRecord(ByteBuffer buffer) {
        status = StatusType.byCode(buffer.get());
        activity = ActivityType.byCode(buffer.get());
        timestamp = Instant.ofEpochSecond(buffer.getInt());
        return this;
    }

}
