package pl.radical.ttbin.records;

import lombok.Getter;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x34, length = 21)
public class SwimRecord implements ITTBinRecord {
    /**
     * Timestamp of this record
     */
    @Getter
    private int timeStamp;

    /**
     * Total distance
     */
    @Getter
    private float totalDistance;

    /**
     * Frequency
     */
    @Getter
    private byte frequency;

    /**
     * Stroke type
     * TODO Missing type
     */
    @Getter
    private byte strokeType;
    /**
     * Strokes - since the last report
     */
    @Getter
    private int strokes;

    /**
     * Completed laps
     */
    @Getter
    private int completedLaps;

    /**
     * Total calories
     */
    @Getter
    private short totalCalories;


    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        timeStamp = buffer.getInt();
        totalDistance = buffer.getFloat();
        frequency = buffer.get();
        strokeType = buffer.get();
        strokes = buffer.getInt();
        completedLaps = buffer.getInt();
        totalCalories = buffer.getShort();
        return this;
    }
}
