package pl.radical.ttbin.records.types;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 25.10.2015
 */
@AllArgsConstructor
@Slf4j
public enum RecoveryStatusType {
    UNDEFINED(-1),
    NONE(0),
    BAD(1),
    DECENT(2),
    GOOD(3),
    EXCELLENT(4);

    private int code;

    public static RecoveryStatusType byCode(int code) {
        for (RecoveryStatusType type : RecoveryStatusType.values()) {
            if (type.code == code) {
                return type;
            }
        }
        log.warn("Unknown RecoveryStatusType was requested! [{}]", code);
        return UNDEFINED;
    }

}
