package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x23, length = 20)
@ToString
@Slf4j
// TODO Rename to satellite data related name
public class GPSSatelliteRecord implements ITTBinRecord {
    @Getter
    private int bufferPos;

    /**
     * dilution of precision, seems to be in metres * 10^7
     */
    @Getter
    private double dop;

    @Getter
    private byte unk1;

    /**
     * up to 4 satellite numbers, satellites currently being used
     */
    @Getter
    private byte[] satellites = new byte[4];

    /**
     * signal strength for each satellite listed above, not sure what units
     * 0 being the best one (full bars), 3 being minimal
     */
    @Getter
    private byte[] snr = new byte[4];

    @Getter
    private byte[] unk2 = new byte[4];

    @Getter
    private byte[] unk3 = new byte[2];

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        bufferPos = buffer.position() - 1;

        dop = buffer.getInt() / 10000000;
        unk1 = buffer.get();

        satellites[0] = buffer.get();
        satellites[1] = buffer.get();
        satellites[2] = buffer.get();
        satellites[3] = buffer.get();

        snr[0] = buffer.get();
        snr[1] = buffer.get();
        snr[2] = buffer.get();
        snr[3] = buffer.get();

        unk2[0] = buffer.get();
        unk2[1] = buffer.get();
        unk2[2] = buffer.get();
        unk2[3] = buffer.get();

        unk3[0] = buffer.get();
        unk3[1] = buffer.get();

        log.debug("Satellite data record: {}", this);
        return this;
    }
}
