package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x22, length = 28)
@ToString
public class GPSRecord implements ITTBinRecord {
    @Getter
    private int bufferPos;

    /**
     * Latitude (degrees * 1e7)
     */
    @Getter
    private int latitude;
    /**
     * Longitude (degrees * 1e7)
     */
    @Getter
    private int longitude;

    /**
     * Heading (degrees * 100, N = 0, E = 9000)
     */
    @Getter
    private short heading;

    /**
     * GPS  speed (cm/s)
     */
    @Getter
    private short gpsSpeed;

    /**
     * Timestamp (gps time (utc))
     */
    @Getter
    private int timestamp;

    /**
     * Calories
     */
    @Getter
    private short calories;

    /**
     * Instant speed (m/s)
     */
    @Getter
    private float instantSpeed;

    /**
     * Cumulative distance (metres)
     */
    @Getter
    private float cumDistance;

    /**
     * Cycles (running = steps/sec, cycling = crank rpm)
     */
    @Getter
    private byte cycles;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        bufferPos = buffer.position() - 1;

        latitude = buffer.getInt();
        longitude = buffer.getInt();
        heading = buffer.getShort();
        gpsSpeed = buffer.getShort();
        timestamp = buffer.getInt();
        calories = buffer.getShort();
        instantSpeed = buffer.getFloat();
        cumDistance = buffer.getFloat();
        cycles = buffer.get();

        return this;
    }
}
