package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;
import java.time.Instant;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x25, length = 7)
@ToString
public class FileHeartRecord implements ITTBinRecord {
    /**
     * Heart rate (bpm)
     */
    @Getter
    private int heartRate;

    /**
     * Timestamp (local time)
     */
    @Getter
    private Instant timeStamp;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        heartRate = buffer.get() & 0xFF;
        buffer.get();
        timeStamp = Instant.ofEpochSecond(buffer.getInt());
        return this;
    }
}
