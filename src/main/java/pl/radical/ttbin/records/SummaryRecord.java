package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.ActivityType;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x27, length = 14)
@ToString
@Slf4j
public class SummaryRecord implements ITTBinRecord {

    /**
     * Activity type
     */
    @Getter
    private ActivityType activity;

    /**
     * Distance
     */
    @Getter
    private float distance;

    /**
     * Duration (seconds, after adding 1)
     */
    @Getter
    private int duration;

    /**
     * Calories
     */
    @Getter
    private short calories;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        activity = ActivityType.byCode(buffer.get());
        distance = buffer.getFloat();
        duration = buffer.getInt();
        calories = buffer.getShort();
        log.info("Unknown part of the record: {}", buffer.getShort());
        return this;
    }
}
