package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;
import java.time.Instant;
import java.util.stream.IntStream;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x40, length = 12)
@ToString
@Slf4j
// Seems to be only recorded in Indoor Cycling
public class UnknownRecord0x40 implements ITTBinRecord {
    @Getter
    private int bufferPosition;

    @Getter
    private Instant timestamp;

    @Getter
    private byte[] data;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        bufferPosition = buffer.position() - 1;
        timestamp = Instant.ofEpochSecond(buffer.getInt());
        data = new byte[7];
        IntStream.range(0, data.length).forEach(i -> data[i] = buffer.get());
        return this;
    }
}
