package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.IntervalType;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3a, length = 2)
@ToString
public class IntervalStartRecord implements ITTBinRecord {
    /**
     * Type of interval
     */
    @Getter
    private IntervalType type;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        type = IntervalType.byCode(buffer.get());
        return this;
    }
}
