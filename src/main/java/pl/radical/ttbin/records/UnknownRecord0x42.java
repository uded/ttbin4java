package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x42, length = 2)
@ToString
@Slf4j
public class UnknownRecord0x42 implements ITTBinRecord {
    @Getter
    private byte data;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        data = buffer.get();
        return this;
    }
}
