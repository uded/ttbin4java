package pl.radical.ttbin.records.types;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 25.10.2015
 */
@AllArgsConstructor
@Slf4j
public enum IntervalType {
    UNKNOWN(-1),
    WARMUP(1),
    WORK(2),
    REST(3),
    COOLDOWN(4),
    FINISHED(5);

    private int code;

    public static IntervalType byCode(int code) {
        for (IntervalType type : IntervalType.values()) {
            if (type.code == code) {
                return type;
            }
        }
        log.warn("Unknown IntervalType was requested! [{}]", code);
        return UNKNOWN;
    }

}
