package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;
import java.time.Instant;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x41, length = 11)
@Slf4j
@ToString
public class GymActivityRecord implements ITTBinRecord {
    /**
     * Looks like a timestamp, so it's possibly is a timestamp
     */
    @Getter
    private Instant timeStamp;

    /**
     * Caloried burned since beginning of the exercise
     */
    @Getter
    private short calories;

    /**
     * Cycles since beginning of the exercise
     */
    @Getter
    private int cycles;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        timeStamp = Instant.ofEpochSecond(buffer.getInt());
        calories = buffer.getShort();
        cycles = buffer.getInt();
        return this;
    }
}
