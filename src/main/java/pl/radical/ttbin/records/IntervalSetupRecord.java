package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@ToString
@Record(tag = 0x39, length = 22)
public class IntervalSetupRecord implements ITTBinRecord {
    /**
     * Warm up type
     * 0 = ditance, 1 = time
     */
    @Getter
    byte warmUpType;

    /**
     * Warm up length (metres or seconds, depending on type)
     */
    @Getter
    int warmUpLength;

    /**
     * Work type
     * 0 = ditance, 1 = time
     */
    @Getter
    byte workType;

    /**
     * Work length (metres or seconds, depending on type)
     */
    @Getter
    int workLength;

    /**
     * Rest type
     * 0 = ditance, 1 = time
     */
    @Getter
    byte restType;

    /**
     * Rest length (metres or seconds, depending on type)
     */
    @Getter
    int restLength;

    /**
     * Cool down type
     * 0 = ditance, 1 = time
     */
    @Getter
    byte collDownType;

    /**
     * Cool down length (metres or seconds, depending on type)
     */
    @Getter
    int collDownLength;

    /**
     * Number of sets
     */
    @Getter
    byte sets;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        warmUpType = buffer.get();
        warmUpLength = buffer.getInt();

        workType = buffer.get();
        workLength = buffer.getInt();

        restType = buffer.get();
        restLength = buffer.getInt();

        collDownType = buffer.get();
        collDownLength = buffer.getInt();

        sets = buffer.get();
        return this;
    }
}
