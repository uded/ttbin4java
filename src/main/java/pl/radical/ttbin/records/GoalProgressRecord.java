package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x35, length = 6)
@ToString
public class GoalProgressRecord implements ITTBinRecord {
    @Getter
    private byte percentage;
    @Getter
    private int value;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        percentage = buffer.get();
        value = buffer.getInt();
        return this;
    }
}
