package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.TrainingType;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@ToString
@Record(tag = 0x2d, length = 10)
public class TrainingSetupRecord implements ITTBinRecord {
    @Getter
    private TrainingType trainingType;

    @Getter
    float min;
    @Getter
    float max;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        trainingType = TrainingType.byCode(Byte.toUnsignedInt(buffer.get()));
        min = buffer.getFloat();
        max = buffer.getFloat();

        return this;
    }
}
