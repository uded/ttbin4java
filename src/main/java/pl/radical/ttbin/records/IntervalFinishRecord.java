package pl.radical.ttbin.records;

import lombok.Getter;
import lombok.ToString;
import pl.radical.ttbin.ITTBinRecord;
import pl.radical.ttbin.records.types.IntervalType;

import java.nio.ByteBuffer;

/**
 * @author <a href="mailto:lukasz.rzanek@radical.com.pl">Łukasz Rżanek</a>
 * @since 24.10.2015
 */
@Record(tag = 0x3b, length = 12)
@ToString
public class IntervalFinishRecord implements ITTBinRecord {
    /**
     * Type of the interval that was finished
     */
    @Getter
    private IntervalType type;

    /**
     * Total time
     */
    @Getter
    private int totalTime;

    /**
     * Total distance (in meters)
     */
    @Getter
    private float distance;

    /**
     * Total calories burned
     */
    @Getter
    private short totalCalories;

    @Override
    public ITTBinRecord readRecord(ByteBuffer buffer) {
        type = IntervalType.byCode(buffer.get());
        totalTime = buffer.getInt();
        distance = buffer.getFloat();
        totalCalories = buffer.getShort();
        return this;
    }
}
